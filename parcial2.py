from flask import Flask, jsonify
from flask_cors import CORS, cross_origin


app = Flask(_name_) 

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
from datos import datos


@app.route('/ping',methods=['GET'])
def ping():
    return 'Pong!'

@app.route('/data',methods=['GET'])
@cross_origin()
def data():
    return jsonify({'data':datos})
    
    
@app.route('/inicio',methods=['GET'])
@cross_origin()
def inicio():
    iniciovac = []
    finvac = []
    
    for dato in datos:
        inivac.append(dato['field25']) 
        finvac.append(dato['field64'])
  
    
    
    return jsonify(
        
        {
            'fechaprimvac':inivac[0],
            'primvacporcentaje':inivac[1],
            'fechaultvac':finvac[1],
            'ultvacporcentaje':finvac[0]
        })


@app.route('/ultimosvacunados',methods=['GET'])
@cross_origin()
def ultimos10():


    ultimosvac = []
    fechaultimosvac=[]
    
    i=0
    
    for dato in datos:
        if i==0:
            
            for x in range(54, 64):
                fechaultimosvac.append(dato['field'+str(x)])
            
        
        if i==1:
            
             for x in range(54, 64):
                ultimosvac.append(dato['field'+str(x)])
                
        i=i+1      
    
    return jsonify(
        
        {
            
            'ultimas10vacsporcentaje':fechaultimosvac,
            'fechaultimas10vacs':ultimosvac,

        })
    
    

@app.route('/primerosvacunados',methods=['GET'])
@cross_origin()
def primeros10():
    
    primvacs = []
    fechaprimvacs=[]
    
    i=0
    
    for dato in datos:
        if i==0:
            
            for x in range(25, 35):
                fechaprimvacs.append(dato['field'+str(x)])
            
        
        if i==1:
            
             for x in range(25, 35):
                primvacs.append(dato['field'+str(x)])
                
        i=i+1      
    
    return jsonify(
        
        {
            
            'prim10vacsporcentaje':primvacs,
            'fecha10primvacs':fechaprimvacs,

        })

if _name_ == '_main_':
    app.run(debug=True,port=4000)

